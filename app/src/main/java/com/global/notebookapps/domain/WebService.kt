package com.global.notebookapps.domain

import com.global.notebookapps.ui.model.NotebookList
import retrofit2.http.GET
import retrofit2.http.Header

interface WebService {

    @GET("list")
    suspend fun fetchNotebookList(@Header("Content-type") content_type: String) : NotebookList

}
package com.global.notebookapps.domain

import com.global.notebookapps.ui.model.Notebook
import com.global.notebookapps.vo.Resource

interface Repo {
    suspend fun fetchNotebookList(content_type : String) : Resource<List<Notebook>>
}
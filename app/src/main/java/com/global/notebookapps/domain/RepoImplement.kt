package com.global.notebookapps.domain

import com.global.notebookapps.data.NotebookDataSource
import com.global.notebookapps.ui.model.Notebook
import com.global.notebookapps.vo.Resource

class RepoImplement(private val dataSource: NotebookDataSource) : Repo{
    override suspend fun fetchNotebookList(content_type : String): Resource<List<Notebook>> {
        return dataSource.fetchNotebookList(content_type)
    }
}
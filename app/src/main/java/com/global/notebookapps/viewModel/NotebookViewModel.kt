package com.global.notebookapps.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.global.notebookapps.domain.Repo
import com.global.notebookapps.vo.Resource
import kotlinx.coroutines.Dispatchers
import java.lang.Exception

class NotebookViewModel(private val repo: Repo) : ViewModel() {

    fun fetchNotebookList(content_type : String) = liveData(Dispatchers.IO) {
        emit(Resource.Loading())
        try {
            emit(repo.fetchNotebookList(content_type))
        }catch (e:Exception){
            emit(Resource.Failure(e))
        }
    }
}
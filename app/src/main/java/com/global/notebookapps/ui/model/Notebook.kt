package com.global.notebookapps.ui.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Notebook(
    @SerializedName("title")
    val title : String = "",
    @SerializedName("description")
    val description : String = "",
    @SerializedName("image")
    val image : String = ""
) : Parcelable

data class NotebookList(
    val notebookList: List<Notebook> = listOf()
)
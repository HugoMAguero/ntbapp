package com.global.notebookapps.ui.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.global.notebookapps.R
import com.global.notebookapps.data.NotebookDataSource
import com.global.notebookapps.domain.RepoImplement
import com.global.notebookapps.ui.adapter.NotebookAdapter
import com.global.notebookapps.ui.model.Notebook
import com.global.notebookapps.viewModel.NotebookViewModel
import com.global.notebookapps.viewModel.VMFactory
import com.global.notebookapps.vo.Resource
import kotlinx.android.synthetic.main.fragment_main.*

class MainFragment : Fragment(), NotebookAdapter.OnNotebookClick {

    private val viewModel by activityViewModels<NotebookViewModel> { VMFactory(
        RepoImplement(
            NotebookDataSource()
        )
    ) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view : View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
        val content_type = "application/json"
        viewModel.fetchNotebookList(content_type).observe(viewLifecycleOwner, Observer { result ->
            when(result) {
                is Resource.Loading -> {
                    progressBar.visibility = View.VISIBLE
                }
                is Resource.Success -> {
                    progressBar.visibility = View.GONE
                    rv_notebook.adapter = NotebookAdapter(requireContext(), result.data, this)
                }
                is Resource.Failure -> {
                    progressBar.visibility = View.GONE
                }
            }
        })
    }

    private fun setupRecyclerView() {
        rv_notebook.layoutManager = LinearLayoutManager(requireContext())
        rv_notebook.addItemDecoration(
            DividerItemDecoration(
                requireContext(),
                DividerItemDecoration.VERTICAL
            )
        )
    }

    override fun onClick(notebook: Notebook) {
        val bundle = Bundle()
        bundle.putParcelable("notebook", notebook)
        findNavController().navigate(R.id.action_mainFragment_to_detailFragment, bundle)
    }
}
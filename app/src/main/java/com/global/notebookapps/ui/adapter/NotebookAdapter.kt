package com.global.notebookapps.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.global.notebookapps.R
import com.global.notebookapps.base.BaseViewHolder
import com.global.notebookapps.ui.model.Notebook
import kotlinx.android.synthetic.main.item_notebook.view.*

class NotebookAdapter(private val context: Context, private val notebookList: List<Notebook>, private val itemClick: OnNotebookClick) : RecyclerView.Adapter<BaseViewHolder<*>>() {

    interface OnNotebookClick {
        fun onClick(notebook: Notebook)
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        return NotebookViewHolder(LayoutInflater.from(context).inflate(R.layout.item_notebook, parent, false))
    }

    override fun getItemCount(): Int {
        return notebookList.size
    }

    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int) {
        when(holder) {
            is NotebookViewHolder -> {
                holder.bind(notebookList[position], position)
            }
        }
    }

    inner class NotebookViewHolder(itemView: View) : BaseViewHolder<Notebook>(itemView){
        override fun bind(item: Notebook, position: Int) {
            itemView.title_notebook.text = item.title
            Glide.with(context).load(item.image).centerCrop().into(itemView.imageNotebook)
            itemView.setOnClickListener {
                itemClick.onClick(item)
            }
        }
    }
}
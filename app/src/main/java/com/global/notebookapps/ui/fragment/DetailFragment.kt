package com.global.notebookapps.ui.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.global.notebookapps.R
import com.global.notebookapps.ui.model.Notebook
import kotlinx.android.synthetic.main.fragment_detail.*
import kotlinx.android.synthetic.main.item_notebook.*
import kotlinx.android.synthetic.main.item_notebook.view.*


class DetailFragment : Fragment() {

    private lateinit var notebook: Notebook

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requireArguments().let {
            notebook = it.getParcelable("notebook")!!
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        title_detail.text = notebook.title
        text_instructions_detail.text = notebook.description
        Glide.with(requireContext()).load(notebook.image).centerCrop().into(imageNotebookDetail)
    }
}
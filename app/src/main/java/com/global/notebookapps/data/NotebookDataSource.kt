package com.global.notebookapps.data

import com.global.notebookapps.ui.model.Notebook
import com.global.notebookapps.ui.model.NotebookList
import com.global.notebookapps.vo.Resource
import com.global.notebookapps.vo.RetrofitClient

class NotebookDataSource () {
    suspend fun fetchNotebookList(content_type : String): Resource<List<Notebook>> {
        return Resource.Success(RetrofitClient.webService.fetchNotebookList(content_type).notebookList)
    }
}